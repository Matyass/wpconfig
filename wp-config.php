<?php
$table_prefix = 'test_';

define('DB_NAME', 'myapp');
define('DB_USER', 'dev');
define('DB_PASSWORD', 'dev');
define('DB_HOST', 'db');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

define('WP_HOME',    'http://local.example.cz');
define('WP_SITEURL', 'http://local.example.cz');

define('WPLANG', 'cs_CZ');
define('COOKIE_DOMAIN', 'local.example.cz');
define('WP_CONTENT_URL', 'http://local.example.cz/wp-content');

define('WP_DEBUG', true); // na produkci musi byt FALSE
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', false);
define('WP_CACHE', false);
define('DISALLOW_FILE_EDIT', true);

define('FTP_USER', '');
define('FTP_PASS', '');
define('FTP_HOST', '');
